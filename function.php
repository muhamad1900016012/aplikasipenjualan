<?php
    //koneksi database
    session_start();
    $conn = mysqli_connect ("localhost","root","","adminaros");

    ////tambah produk
    if(isset($_POST['addproduk'])){
        $namaproduk = $_POST['namaproduk'];
        $jenis = $_POST['jenis'];
        $model = $_POST['model'];
        $harga = $_POST['harga'];

        $addtotable = mysqli_query ( $conn, "insert into produk(namaproduk, jenis, model,harga )values ('$namaproduk','$jenis','$model','$harga')");
        if($addtotable){
            header('location:produk.php');
        }else{
            echo "gagal";
        }
    }
    ////tambah stock
    if(isset($_POST['addnewbarang'])){
        $namabarang = $_POST['namabarang'];
        $deskripsi = $_POST['deskripsi'];
        $stock = $_POST['stock'];

        $addtotable = mysqli_query ( $conn, "insert into stock(namabarang, deskripsi, stock )values ('$namabarang','$deskripsi','$stock')");
        if($addtotable){
            header('location:index.php');
        }else{
            echo "gagal";
        }
    }

    ////barang masuk

    if (isset($_POST['barangmasuk'])){
        $barang = $_POST['barang'];
        $penerima =$_POST['penerima'];
        $jumlah = $_POST['jumlah'];

        $datastock = mysqli_query($conn, "select * from stock where idbarang='$barang'");
        $datastock2 = mysqli_fetch_array($datastock);

        $finalstock = $datastock2['stock'];
        $totalstock = $finalstock + $jumlah;

        $addtomasuk = mysqli_query ($conn, "insert into masuk (idbarang,keterangan,jumlah) values ('$barang','$penerima','$jumlah')");
        $updatestock = mysqli_query($conn, "update stock set stock = '$totalstock' where idbarang='$barang'");
        if($addtomasuk&&$updatestock){
            header('location:masuk.php');
        }else{
            echo "gagal";
        }
    }


    ///barang keluar
    if (isset($_POST['barangkeluar'])){
        $barang = $_POST['barang'];
        $penerima =$_POST['penerima'];
        $jumlah = $_POST['jumlah'];

        $datastock = mysqli_query($conn, "select * from stock where idbarang='$barang'");
        $datastock2 = mysqli_fetch_array($datastock);

        $finalstock = $datastock2['stock'];
        $totalstock = $finalstock - $jumlah;

        $addtokeluar = mysqli_query ($conn, "insert into keluar (idbarang,penerima,jumlah) values ('$barang','$penerima','$jumlah')");
        $updatestock = mysqli_query($conn, "update stock set stock = '$totalstock' where idbarang='$barang'");
        if($addtokeluar&&$updatestock){
            header('location:keluar.php');
        }else{
            echo "gagal";
        }
    }

    ////penjualan 
    if (isset($_POST['penjualan'])){
        $namaproduk = $_POST['jual'];
        $jenis = $_POST['jenis'];
        $username = $_POST['username'];
        $jumlah =$_POST['jumlah'];
        $harga =$_POST['harga'];
    

        if($jenis =="Lampu Akrilik Kayu")
        {
            $ambildatakayu = mysqli_query($conn, "select * from stock where namabarang='kayu' ");
            $datakayu = mysqli_fetch_array($ambildatakayu);
            $datakayu2 = $datakayu['stock'];
            $updatekayu = $datakayu2 - 1;
            $updatedatabasekayu = mysqli_query($conn, "update stock set stock='$updatekayu' where namabarang='kayu'");

            $ambildatausb = mysqli_query($conn, "select * from stock where namabarang='usb' ");
            $datausb = mysqli_fetch_array($ambildatausb);
            $datausb2 = $datausb['stock'];
            $updateusb = $datausb2 - 1;
            $updatadatabaseusb = mysqli_query($conn, "update stock set stock='$updateusb' where namabarang='usb'");

            $ambildatakontroler = mysqli_query($conn, "select * from stock where namabarang='kontroler' ");
            $datakontroler = mysqli_fetch_array($ambildatakontroler);
            $datakontroler2 = $datakontroler['stock'];
            $updatekontroler = $datakontroler2 - 1;
            $updatadatabasekontroler = mysqli_query($conn, "update stock set stock='$updatekontroler' where namabarang='kontroler'");

            $ambildataakrilik = mysqli_query($conn, "select * from stock where namabarang='akrilik' ");
            $dataakrilik = mysqli_fetch_array($ambildataakrilik);
            $dataakrilik2 = $dataakrilik['stock'];
            $updatedataakrilik = $dataakrilik2 - 1;
            $updatadatabaseakrilik = mysqli_query($conn, "update stock set stock='$updatedataakrilik' where namabarang='akrilik'");

            
            $ambildatakardus = mysqli_query($conn, "select * from stock where namabarang='kardus' ");
            $datakardus = mysqli_fetch_array($ambildatakardus);
            $datakardus2 = $datakardus['stock'];
            $updatekardus = $datakardus2 - 1;
            $updatadatabasekardus = mysqli_query($conn, "update stock set stock='$updatekardus' where namabarang='kardus'");
        }
        if($jenis =="Lampu Akrilik Lingkaran")
        {
            $ambildatakardus = mysqli_query($conn, "select * from stock where namabarang='kardus' ");
            $datakardus = mysqli_fetch_array($ambildatakardus);
            $datakardus2 = $datakardus['stock'];
            $updatekardus = $datakardus2 - 1;
            $updatadatabasekardus = mysqli_query($conn, "update stock set stock='$updatekardus' where namabarang='kardus'");

            $ambildataakrilik = mysqli_query($conn, "select * from stock where namabarang='akrilik' ");
            $dataakrilik = mysqli_fetch_array($ambildataakrilik);
            $dataakrilik2 = $dataakrilik['stock'];
            $updatedataakrilik = $dataakrilik2 - 1;
            $updatadatabaseakrilik = mysqli_query($conn, "update stock set stock='$updatedataakrilik' where namabarang='akrilik'");
        }

        $total = $harga * $jumlah;

        $addtotable = mysqli_query ( $conn, "insert into penjualan(pesanan, jenis, username,harga )values ('$namaproduk','$jenis','$username','$total')");
        if($addtotable){
            header('location:penjualan.php');
        }else{
            echo "gagal";
        }
    }

    //update barang
    if(isset($_POST['updatebarang'])){
        $idbarang = $_POST['idb'];
        $namabarang = $_POST['namabarang'];
        $deskripsi = $_POST['deskripsi'];

        $update = mysqli_query($conn, " update stock set namabarang='$namabarang', deskripsi='$deskripsi' where idbarang='$idbarang'");
        if ($update){
            header('location:index.php');
        }else{
            header('location:index.php');
        }

    }
    ////hapus barang 
    if(isset($_POST['hapusbarang'])){
        $idbarang =$_POST['idb'];

        $hapus = mysqli_query($conn, "delete from stock where idbarang='$idbarang'");
        if ($hapus){
            header('location:index.php');
        }else{
            header('location:index.php');
        }
    }

    //// update barang masuk
    if(isset($_POST['updatemasuk'])){
        $idb = $_POST['idb'];
        $idm = $_POST['idm'];
        $namabarang = $_POST['namabarang'];
        $deskripsi =$_POST['keterangan'];
        $jumlah = $_POST['jumlah'];
        
        $cekstok = mysqli_query($conn, " select * from stock where idbarang='$idb'");
        $cekstock2 = mysqli_fetch_array($cekstok);
        $stockfinal = $cekstock2['stock'];

        $stocknow= mysqli_query($conn, "select * from masuk where idmasuk = '$idm'");
        $stocknow1 = mysqli_fetch_array($stocknow);
        $stocknow2 = $stocknow1['jumlah'];

        if($jumlah>$stocknow2){
            $selisih = $jumlah - $stocknow2;
            $stockbahan = $stockfinal - $selisih;
            $stockterbaru = mysqli_query($conn, "update stock set stock = '$stockbahan' where idbarang ='$idb'");
            $stockmasukbaru = mysqli_query($conn, "update masuk set jumlah = '$jumlah'   where idmasuk='$idm'");

                if($stockterbaru&&$stockmasukbaru){
                    header('location:masuk.php');
                }else{
                    header('location:masuk.php');
                }
                
        }else{
            $selisih =  $stocknow2-$jumlah;
            $stockbahan = $stockfinal - $selisih;
            $stockterbaru = mysqli_query($conn, "update stock set stock = '$stockbahan' where idbarang ='$idb'");
            $stockmasukbaru = mysqli_query($conn, "update masuk set jumlah = '$jumlah' where idmasuk='$idm'");

            if($stockterbaru&&$stockmasukbaru){
                header('location:masuk.php');
            }else{
                header('location:masuk.php');
            }
        }
    }
    ////hapus barang masuk 
    if(isset($_POST['hapusmasuk'])){
        $idb=$_POST['idb'];
        $jumlah = $_POST['jumlah'];
        $idm = $_POST['idm'];

        $getdatastock = mysqli_query($conn, "select * from stock where idbarang='$idb'");
        $data = mysqli_fetch_array($getdatastock);
        $stock = $data['stock'];

        $selisih = $stock-$jumlah;

        $update = mysqli_query($conn, " update stock set stock ='$selisih' where idbarang='$idb'" );
        $hapusdata = mysqli_query($conn, "delete from masuk where idmasuk='$idm'");
        if($update&&$hapusdata){
            header('location:masuk.php');
        }else{
            header('location:masuk.php');
        }
    }

    //update barang
    if(isset($_POST['updatebarang'])){
        $idbarang = $_POST['idb'];
        $namabarang = $_POST['namabarang'];
        $deskripsi = $_POST['deskripsi'];

        $update = mysqli_query($conn, " update stock set namabarang='$namabarang', deskripsi='$deskripsi' where idbarang='$idbarang'");
        if ($update){
            header('location:index.php');
        }else{
            header('location:index.php');
        }

    }
    ////hapus barang 
    if(isset($_POST['hapusbarang'])){
        $idbarang =$_POST['idb'];

        $hapus = mysqli_query($conn, "delete from stock where idbarang='$idbarang'");
        if ($hapus){
            header('location:index.php');
        }else{
            header('location:index.php');
        }
    }

    //// update barang keluar
    if(isset($_POST['updatekeluar'])){
        $idb = $_POST['idb'];
        $idk = $_POST['idk'];
        $namabarang = $_POST['namabarang'];
        $deskripsi =$_POST['penerima'];
        $jumlah = $_POST['jumlah'];
        
        $cekstok = mysqli_query($conn, " select * from stock where idbarang='$idb'");
        $cekstock2 = mysqli_fetch_array($cekstok);
        $stockfinal = $cekstock2['stock'];

        $stocknow= mysqli_query($conn, "select * from keluar where idkeluar = '$idk'");
        $stocknow1 = mysqli_fetch_array($stocknow);
        $stocknow2 = $stocknow1['jumlah'];

        if($jumlah>$stocknow2){
            $selisih = $jumlah - $stocknow2;
            $stockbahan = $stockfinal - $selisih;
            $stockterbaru = mysqli_query($conn, "update stock set stock = '$stockbahan' where idbarang ='$idb'");
            $stockmasukbaru = mysqli_query($conn, "update keluar set jumlah = '$jumlah'   where idkeluar='$idk'");

                if($stockterbaru&&$stockmasukbaru){
                    header('location:keluar.php');
                }else{
                    header('location:keluar.php');
                }
                
        }else{
            $selisih =  $stocknow2-$jumlah;
            $stockbahan = $stockfinal + $selisih;
            $stockterbaru = mysqli_query($conn, "update stock set stock = '$stockbahan' where idbarang ='$idb'");
            $stockmasukbaru = mysqli_query($conn, "update keluar set jumlah = '$jumlah'   where idkeluar='$idk'");

            if($stockterbaru&&$stockmasukbaru){
                header('location:keluar.php');
            }else{
                header('location:keluar.php');
            }
        }
    }
    ////hapus barang keluar
    if(isset($_POST['hapuskeluar'])){
        $idk  = $_POST['idk'];
        $jumlah = $_POST['jumlah'];
        $idb = $_POST['idb'];

        $getdatastock = mysqli_query($conn, "select * from stock where idbarang='$idb'");
        $data = mysqli_fetch_array($getdatastock);
        $stock = $data['stock'];

        $selisih = $stock+$jumlah;

        $update = mysqli_query($conn, " update stock set stock ='$selisih' where idbarang='$idb'" );
        $hapusdata = mysqli_query($conn, "delete from keluar where idkeluar='$idk'");
        if($update&&$hapusdata){
            header('location:keluar.php');
        }else{
            header('location:keluar.php');
        }
    }

    ///update status pesanan 
    if (isset($_POST['updatestatus'])){
        $nmr = $_POST['nmr'];
        $updatestatus =mysqli_query($conn, "update penjualan set status='selesai' where no='$nmr'");
        if($updatestatus){
            header('location:penjualan.php');
        }else{
            header('location:index.php');
        }
    }
     ///update status pesanan 
     if (isset($_POST['batalkan'])){
        $nmr = $_POST['nmr'];
        $updatestatus =mysqli_query($conn, "delete from penjualan where no='$nmr'");
        if($updatestatus){
            header('location:penjualan.php');
        }else{
            header('location:index.php');
        }
    }
 
     ////tambah kas
     if(isset($_POST['kasmasuk'])){
        $deskripsi = $_POST['deskripsi'];
        $penerima = $_POST['penerima'];
        $jumlah = $_POST['jumlah'];

        $addtotable = mysqli_query ( $conn, "insert into kasmasuk(deskripsi, penerima, jumlah )values ('$deskripsi','$penerima','$jumlah')");
        if($addtotable){
            header('location:kasmasuk.php');
        }else{
            echo "gagal";
        }
    }
    ///update kass
    if(isset($_POST['updatekas'])){
        $idk = $_POST['idk'];
        $deskripsi = $_POST['deskripsi'];
        $penerima = $_POST['penerima'];
        $jumlah    =$_POST['jumlah'];

        $update = mysqli_query($conn, " update kasmasuk set deskripsi='$deskripsi', penerima='$penerima', jumlah='$jumlah' where idkas='$idk'");
        if ($update){
            header('location:kasmasuk.php');
        }else{
            header('location:kasmasuk.php');
        }
    }
    ////hapus kass
    if(isset($_POST['hapuskas'])){
        $idk = $_POST['idk'];
   

        $update = mysqli_query($conn, "delete from kasmasuk where idkas='$idk'");
        if ($update){
            header('location:kasmasuk.php');
        }else{
            header('location:kasmasuk.php');
        }
    }
     ////tambah pengeluaran
     if(isset($_POST['kaskeluar'])){
        $deskripsi = $_POST['deskripsi'];
        $penerima = $_POST['penerima'];
        $jumlah = $_POST['jumlah'];

        $addtotable = mysqli_query ( $conn, "insert into kaskeluar(deskripsi, penerima, jumlah )values ('$deskripsi','$penerima','$jumlah')");
        if($addtotable){
            header('location:kaskeluar.php');
        }else{
            echo "gagal";
        }
    }
    
 ///update kas keluar
 if(isset($_POST['updatekeluar'])){
    $idk = $_POST['idk'];
    $deskripsi = $_POST['deskripsi'];
    $penerima = $_POST['penerima'];
    $jumlah    =$_POST['jumlah'];

    $update = mysqli_query($conn, " update kaskeluar set deskripsi='$deskripsi', penerima='$penerima', jumlah='$jumlah' where idkeluar='$idk'");
    if ($update){
        header('location:kaskeluar.php');
    }else{
        header('location:kaskeluar.php');
    }
}
  ////hapus kass
  if(isset($_POST['hapuskeluar'])){
    $idk = $_POST['idk'];


    $update = mysqli_query($conn, "delete from kaskeluar where idkeluar='$idk'");
    if ($update){
        header('location:kaskeluar.php');
    }else{
        header('location:kaskeluar.php');
    }  
}
 ////hapus kass
 if(isset($_POST['hapusproduk'])){
    $idk = $_POST['nmr'];


    $update = mysqli_query($conn, "delete from produk where no='$idk'");
    if ($update){
        header('location:produk.php');
    }else{
        header('location:produk.php');
    }
}
    ///update status pesanan 
    if (isset($_POST['updatestatushome'])){
        $nmr = $_POST['nmr'];
        $updatestatus =mysqli_query($conn, "update penjualan set status='selesai' where no='$nmr'");
        if($updatestatus){
            header('location:index.php');
        }else{
            header('location:index.php');
        }
    }
     ///update status pesanan 
     if (isset($_POST['batalkanhome'])){
        $nmr = $_POST['nmr'];
        $updatestatus =mysqli_query($conn, "delete from penjualan where no='$nmr'");
        if($updatestatus){
            header('location:index.php');
        }else{
            header('location:index.php');
        }
    }
 
?>